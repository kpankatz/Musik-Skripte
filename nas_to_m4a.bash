#!/bin/bash

source /Users/klauspankatz/Skripte/util/parallel_processes.bash

xld=/Users/klauspankatz/Skripte/xld

tmp=/Users/klauspankatz/Music/tmp

# Number of parallel processes
N=2
open_semaphore $N

[[ -f albums.txt ]] || ssh pankatz@192.168.0.20 "export LANG=de_DE.UTF-8; beet ls -ap 2> /dev/null" 2> /dev/null > albums.txt

while IFS= read -r dir 
do
  #echo "$dir"
  mac_dir="${dir/volume1/Volumes}"
  echo "$mac_dir"
  [[ -d "$mac_dir" ]] || continue
 
  for file in "$mac_dir"/*.flac
  do
    #echo $file
    title="${file##*/}"
    new_title="${title/.flac/.m4a}"
    # Two level basename
    D2=$(dirname "$mac_dir")
    new_dir=$(basename "$D2")/$(basename "$mac_dir")
    [[ -d "$tmp/$new_dir" ]] || mkdir -p "$tmp/$new_dir"
    [[ -f "$tmp/$new_dir/$new_title" ]] || run_with_lock $xld -f aac -o "$tmp/$new_dir" "$file"
  done
done < albums.txt
wait # For processes to finish
