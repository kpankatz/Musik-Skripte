#!/bin/bash
source /Users/klauspankatz/Skripte/util/parallel_processes.bash

xld=/Users/klauspankatz/Skripte/xld

CD=/Users/klauspankatz/Downloads/CD-Rips

# Number of parallel processes
N=2
open_semaphore $N

for dir in $CD/*
do
  tmp=/Users/klauspankatz/Music/tmp/$(basename "$dir")
  echo "$tmp"
  [[ -d "$tmp" ]] || mkdir -p "$tmp"
  for file in "$dir"/*.flac
  do
    echo "$file"
    title="${file##*/}"
    new_title="${title/.flac/.m4a}"
    echo "$new_title"    
    [[ -f "$tmp/$new_title" ]] || run_with_lock $xld -f aac -o  "$tmp" "$file"
  done
done
