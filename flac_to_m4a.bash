#!/bin/bash

source /Users/klauspankatz/Skripte/util/parallel_processes.bash

xld=/Users/klauspankatz/Skripte/xld

Musik="/Users/klauspankatz/Downloads/Transmission/Fertig/./Musik/FLAC"

tmp=/Users/klauspankatz/Music/tmp

itunes="/Users/klauspankatz/Music/iTunes/iTunes Music/Automatically Add to Music.localized"

# Number of parallel processes
N=2
open_semaphore $N

for file in $Musik/**/*.flac
do
  title="${file##*/}"
  new_title="${title/.flac/.m4a}"
  [[ -f "$tmp/$new_title" ]] || run_with_lock $xld -f aac -o "$tmp" "$file"
done

wait # For processes to finish


# import to itunes by moving files into the folder which automatically imports these files 
mv "$tmp"/*.m4a "$itunes"

# Transfer to NAS
rsync -Ruavt --progress --remove-source-files ${Musik}/* pankatz@192.168.0.20:/volume1/Daten


